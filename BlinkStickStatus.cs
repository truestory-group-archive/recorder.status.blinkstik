﻿using Recorder.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Recorder.Core;

namespace Recorder.Status.BlinkStick
{
    public class BlinkStickStatus : IStatusController
    {
        public BlinkStickStatus(IBootstrapper bootstrapper)
        {
            bootstrapper.Exiting += new Bootstrapper.ExitEventHandler(OnExit);
        }

        private void OnExit(object sender, EventArgs e)
        {
            Off();
        }

        // https://github.com/arvydas/BlinkStickDotNet
        public void SetStatus(RecordingStatus status)
        {
            BlinkStickDotNet.BlinkStick[] devices = BlinkStickDotNet.BlinkStick.FindAll();
            if (devices.Length == 0)
                throw new InvalidOperationException("No BlinkStick found!");

            //Iterate through all of them
            foreach (BlinkStickDotNet.BlinkStick device in devices)
            {
                //Open the device
                if (device.OpenDevice())
                {
                    Console.WriteLine(String.Format("Device {0} opened successfully", device.Serial));

                    byte red = 0;
                    byte green = 0;
                    byte blue = 0;

                    if(status == RecordingStatus.Ready)
                    {
                        // Set Green
                        green = 255;
                        red = blue = 0;

                    }else if(status == RecordingStatus.Recording)
                    {
                        // Set Red
                        red = 255;
                        green = blue = 0;

                    }else if(status == RecordingStatus.NotReady)
                    {
                        // Set Yellow
                        red = green = 255;
                        blue = 0;
                    }else if(status == RecordingStatus.Off)
                    {
                        Off();
                        return;
                    }

                    device.SetColor(red, green, blue);
                }
            }
        }

        public void Off()
        {
            BlinkStickDotNet.BlinkStick[] devices = BlinkStickDotNet.BlinkStick.FindAll();
            if (devices.Length == 0)
                throw new InvalidOperationException("No BlinkStick found!");

            //Iterate through all of them
            foreach (BlinkStickDotNet.BlinkStick device in devices)
            {
                if (device.OpenDevice())
                {
                    device.TurnOff();
                }
            }
        }

        ~BlinkStickStatus()
        {
            Off();
        }
    }
}
